<?php
  include("switch_lang.php");
  include("func/function.php");

  require_once 'func/curl.php';
  require_once 'func/Query.php';
  require_once 'func/Lot.php';
  include_once('func/DotENV.php');

  // $new_url = 'https://loyalty.gofx.com/maintenance';
  // header('Location: '.$new_url);

  use DevCoder\DotEnv;
  (new DotEnv('func/.env'))->load();

  $lot = new Lot();
  $curl = new Curl();
  $query = new Query();

  if(empty($_SESSION["email"])){
    $email = 'gofxempty_user@gmail.com';
    $avatar = '';
    $name = '';
  }else{
    $email = $_SESSION['email'];
    $avatar = 'https://secure.gofx.com/images/img_avatar/'.$_SESSION['avatar'];
    $name = $_SESSION['name'];


    // Set Param
    $param['key'] = getenv('API_KEY');
    $param['date'] = getenv('DATE_START');
    $param['reset'] = "true";
    $param['email'] = $email;
    
    $lots = $curl->post('/loyalty/lots', json_encode($param, JSON_UNESCAPED_UNICODE));
    if ($lots != "data empty.") {
      foreach ($lots as $row) {
        $lot->store($row);
      }
    }
    else {
      $row['email'] = $email;
      $row['lots'] = 0.0000;
      $row = json_encode($row);
      $lot->store(json_decode($row));
    }
  }
?>
<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="โปรโมชั่นสุดพิเศษเพื่อตอบแทนเทรดเดอร์ ผู้ใช้บริการของ GOFX.com ยกระดับการลงทุนของคุณให้คุ้มค่ายิ่งขึ้น รับรางวัลมากมายแบบไม่ต้องรอลุ้น ไม่ว่าจะเป็น Gadget สุดทันสมัย ไปจนถึงรถยนต์สุดหรูหลากหลายแบรนด์เช่น Maserati และ Porshe, คูปองเงินสด และของรางวัลอีกมากมายที่เราคัดสรรมาเพื่อคุณโดยเฉพาะ">
    <meta name="keywords" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <meta property="og:title" content="GOFX Loyalty Program l Get exclusive benefits of becoming a GOFX client">
    <meta property="og:description" content="โปรโมชั่นสุดพิเศษเพื่อตอบแทนเทรดเดอร์ ผู้ใช้บริการของ GOFX.com ยกระดับการลงทุนของคุณให้คุ้มค่ายิ่งขึ้น รับรางวัลมากมายแบบไม่ต้องรอลุ้น ไม่ว่าจะเป็น Gadget สุดทันสมัย ไปจนถึงรถยนต์สุดหรูหลากหลายแบรนด์เช่น Maserati และ Porshe, คูปองเงินสด และของรางวัลอีกมากมายที่เราคัดสรรมาเพื่อคุณโดยเฉพาะ">
    <meta property="og:image" content="https://loyalty.gofx.com/images/Thumnail_Loyalty_program.jpg" />
    <meta property="og:image:type" content="image/jpeg">
    <meta property="og:image:width" content="200">
    <meta property="og:image:height" content="200">
    <meta property="og:url" content="">
    <title>GOFX Loyalty Program l Get exclusive benefits of becoming a GOFX client</title>
    <link rel="shortcut icon" href="images/ily.png" />

    <!-- Hotjar Tracking Code for https://loyalty.gofx.com/ -->
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:2486904,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>
    
    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@200;300;400;600;900&display=swap" rel="stylesheet">

    <!-- Css countdown -->
    <link rel="stylesheet" href="../countdown/util.css" />

    <!-- Css Styles -->
    <link rel="stylesheet" href="../css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="../css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="../css/elegant-icons.css" type="text/css">
    <link rel="stylesheet" href="../css/nice-select.css" type="text/css">
    <link rel="stylesheet" href="../css/jquery-ui.min.css" type="text/css">
    <link rel="stylesheet" href="../css/owl.carousel.min.css" type="text/css">
    <link rel="stylesheet" href="../css/slicknav.min.css" type="text/css">
    <link rel="stylesheet" href="../css/style.css?v=<?= strtotime('now'); ?>" type="text/css">
    <link rel="stylesheet" href="../css/loyalty_reward.css?v=<?= strtotime('now'); ?>" type="text/css">
    <link rel="stylesheet" href="../css/rewards.css" type="text/css">
    <link rel="stylesheet" href="../css/animate.min.css" />
    <link rel="stylesheet" href="../css/shield.css?v=<?= strtotime('now'); ?>">
    <link rel="stylesheet" href="../fontawesome5.15.3/css/all.css">
    <link rel="stylesheet" id="elementor-frontend-css" href="https://www.gofx.com/wp-content/plugins/elementor/assets/css/frontend.min.css?ver=3.0.14" type="text/css" media="all">
    <link rel="stylesheet" id="elementor-pro-css" href="https://www.gofx.com/wp-content/plugins/elementor-pro/assets/css/frontend.min.css?ver=3.0.6" type="text/css" media="all">
    <link rel="stylesheet" id="elementor-post-24805-css" href="https://www.gofx.com/wp-content/uploads/elementor/css/post-24805.css?ver=1622607923" type="text/css" media="all">
    <script type="text/javascript" src="https://www.gofx.com/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp" id="jquery-core-js"></script>

    <script src="js/ajax-jquery.min.js"></script>

    <script>
     function changeLang_mobile(){
      document.getElementById('form_lang_mobile').submit();
     }
     function changeLang_desktop(){
      document.getElementById('form_lang_desktop').submit();
     }
     </script>


  <!-- Google Tag Manager -->
  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','GTM-MVHRV4M');</script>
  <!-- End Google Tag Manager -->
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-169650126-3"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-169650126-3');
  </script>

</head>
<?php  //include('social.html'); ?>
<body class="<?php if($page == 'index') {echo 'index-background';}
            if($page == 'rewards') {echo 'reward-background';}
            if($page == 'reward_detail') {echo 'reward-detail-background';}
            if($page == 'profile') {echo 'reward-detail-background';}
            if($page == 'check_in') {echo 'daily-login-background';}
      ?>">

  <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MVHRV4M"
  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <!-- End Google Tag Manager (noscript) -->
 

  <?php
    $GetData = new CheckUser();
    $objGetData = $GetData->fncCheckUser($email);
    $point = $objGetData['point'];
    $rw_check_point = $point;
    $lots = $objGetData['lot'];
    $level_title = $objGetData['level_title'];
    $level_img = $objGetData['level_image'];
 
      if ($point < 0.01) {
          $n_format = 0;
      }
      else if ($point < 1) {
          $n_format = $point[0].$point[1].$point[2].$point[3];
      }
      else if ($point < 10) {
          $n_format = $point[0].$point[1].$point[2].$point[3];
      }
      else if ($point < 100) {
          $n_format = $point[0].$point[1];
      }
      else if ($point < 1000) {
          $n_format = $point[0].$point[1].$point[2];
      }
      else if ($point < 10000) {
          $n = ($point / 1000);
          $num1 = strstr($n, '.', true);
          $num2 = substr($n, strpos($n, ".") + 0,2);
          $n_format = $num1.$num2.'K';
      }
      else if ($point < 1000000) {
          $n = ($point / 1000);
          $num1 = strstr($n, '.', true);
          $num2 = substr($n, strpos($n, ".") + 0,2);
          $n_format = $num1.$num2.'K';
      } else if ($point < 1000000000) {
          $n = ($point / 1000000);
          $num1 = strstr($n, '.', true);
          $num2 = substr($n, strpos($n, ".") + 0,2);
          $n_format = $num1.$num2.'M';
      } else {
          $n = ($point / 1000000000);
          $num1 = strstr($n, '.', true);
          $num2 = substr($n, strpos($n, ".") + 0,2);
          $n_format = $num1.$num2.'B';
      }
  ?>

  <!-- Humberger Begin -->
  <div id="mobile_menu">
    <div class="row">
      <?php if($email == 'gofxempty_user@gmail.com' ){ ?>
        <div class="col-md-3 col-4">
          <a href="index"><img src="/images/logo.png" class="mb-logo" id="mb-logo"></a>
        </div>
        <div class="col-md-3 col-4 mb-reward-logo">
        <img src="images/header_1.png" class="mb-ver">
        </div>
        <div class="col-lg-8 col-md-8 col-4">
        
      </div>
      <?php }else{ ?>
      <div class="col-md-3 col-4">
        <a href="index"><img src="/images/logo.png" class="mb-logo" id="mb-logo"></a>
      </div>
      <div class="col-md-3 col-4 mb-reward-logo">
       <img src="images/header_1.png" class="mb-ver">
      </div>
      <div class="col-lg-8 col-md-8 col-4">
        <p class="mb-point"><img src="images/point.gif" width="40" height="40" alt="" class="avatar rounded-circle"> <?= $n_format; ?></p>
      </div>
      <!-- <div class="col-lg-1 col-md-1 col-2">
        <div class="humberger__open mb-ver-none">
            <i class="fa fa-bars"></i>
        </div>
      </div> -->
      <?php } ?>
    </div>
  </div>

  <div class="menu-mb-ver">
    <div id="mobile_app_menu" class="row row-cols-5">
        <div class="col <? if($page == 'index') {echo 'col-active';}else{echo 'col-link';} ?>">          
          <a href="index" class="<?php if($page == 'index') {echo 'active';}?>">      
            <img src="/images/icon/menu-home-<? if($page == 'index') {echo '2';}else{echo '1';} ?>.png">      
            <?= _MOBILEMENU_INDEX ?>
          </a>
        </div>
        <div class="col <? if($page == 'rewards') {echo 'col-active';}else{echo 'col-link';} ?>">          
          <a href="rewards" class="<?php if($page == 'rewards') {echo 'active';}?>">      
            <img src="/images/icon/menu-reward-<? if($page == 'rewards') {echo '2';}else{echo '1';} ?>.png">      
            <?= _MOBILEMENU_REWARDS ?>
          </a>
        </div> 
        <div class="col col-link">          
          <a href="https://secure.gofx.com/th/newAccount.html" target="_blank">   
            <img src="/images/icon/menu-account-1.png">         
            <?= _MOBILEMENU_ACCOUNT ?>
          </a>
        </div>
        <div class="col <? if($page == 'term-and-conditions') {echo 'col-active';}else{echo 'col-link';} ?>">          
          <a href="term-and-conditions" class="<?php if($page == 'term-and-conditions') {echo 'active';}?>">  
            <img src="/images/icon/menu-help-<? if($page == 'term-and-conditions') {echo '2';}else{echo '1';} ?>.png">          
            <?= _MOBILEMENU_TERMS ?>
          </a>
        </div>
        <div class="col col-link">
          <div class="humberger__open">
              <img src="/images/icon/menu-hambuger-1.png" class="hambuger-mobile">
              <a href="#" class="">
                <?= _MOBILEMENU_NAV ?>
              </a>
          </div>
        </div>
    </div>
  </div>      


      <div class="humberger__menu__overlay"></div>
      <div class="humberger__menu__wrapper">
          <div class="humberger__menu__logo">
              <a  href="index"><img src="/images/logo.png" alt=""></a>
          </div>

          <?php if($email != 'gofxempty_user@gmail.com' ){ ?>
          <div class="humberger__menu__widget">
              <div class="header__top__right__auth">
                <img src="<?= $avatar; ?>" width="40" height="40" alt="" class="avatar rounded-circle">
                <?= $name; ?>
              </div>
          </div>
          <?php } ?>

          <div class="humberger__menu__widget">
              <?php if($email != 'gofxempty_user@gmail.com' ){ ?>
              <div class="header__top__right__auth">
                <img src="images/point.gif" width="40" height="40" alt="" class="avatar rounded-circle">
                <?= $n_format; ?> Point
              </div>
              <?php } ?>
              <div class="header__top__right__auth">
                <!-- <img src="<?php echo $_ENV['APP_STORAGE']; ?>/images/flag/<?= _Flag ?>" width="30" height="30" alt="" class="img-lang"> -->
              </div>
              <div class="header__top__right__auth">
                <!-- Language -->
                <!-- <form method='get' action='' id='form_lang_mobile' >
                  <select name='lang' onchange='changeLang_mobile();' class="switch-lang" >
                  <option value='th' <?php if(isset($_SESSION['lang']) && $_SESSION['lang'] == 'th'){ echo "selected"; } ?> >TH</option>
                  <option value='en' <?php if(isset($_SESSION['lang']) && $_SESSION['lang'] == 'en'){ echo "selected"; } ?> >EN</option>
                 </select>
                </form> -->
              </div>
          </div>

          <nav class="humberger__menu__nav mobile-menu">
              <ul>
                <li><a href="index" class="<?php if($page == 'index') {echo 'humberger__active';}?>"><?= _MAINMENU_INDEX ?></a></li>
                <li><a href="rewards" class="<?php if($page == 'rewards' || $page == 'reward_detail') {echo 'humberger__active';}?>"><?= _MAINMENU_REWARDS ?></a></li>
        
                <li><a href="history" class="<?php if($page == 'history') {echo 'humberger__active';}?>"><?= _MAINMENU_HISTORY ?></a></li>
                <li><a href="term-and-conditions" class="<?php if($page == 'term-and-conditions') {echo 'humberger__active';}?>"><?= _MAINMENU_TERM ?></a></li>
                <?php if($email != 'gofxempty_user@gmail.com' ){ ?>
                <li><a href="profile" class="<?php if($page == 'profile') {echo 'humberger__active';}?>"><?= _PROFILE ?></a></li>
                <?php } ?>
                <li><a href="contact" class="<?php if($page == 'contact') {echo 'humberger__active';}?>"><?= _MAINMENU_CONTACT ?></a></li>
                <?php if($email != 'gofxempty_user@gmail.com' ){ ?>
                <li><a href="logout"><?= _LOGOUT ?></a></li>
                <?php }else{ ?>
                  <li><a href="<?= _SIGNIN_LINK ?>"><?= _SIGNIN ?></a></li>
                  <li><a href="<?= _REGISTER_LINK ?>" target="_blank"><?= _REGISTER ?></a></li>
                <?php } ?>
              </ul>
          </nav>
          <div id="mobile-menu-wrap"></div>
          <div class="header__top__right__social">
              <a href="fb://www.facebook.com/GOFXthailand?_rdc=1&_rdr" target="_bank"><i class="fab fa-facebook-f "></i></a>
              <a href="https://lin.ee/jcDe4w5" target="_bank"><i class="fab fa-line" aria-hidden="true"></i></a>
              <a href="https://www.youtube.com/channel/UCrKNDDHpE1hR3ibZCo6gEmg/featured" target="_bank"><i class="fab fa-youtube"></i></a>
              <a href="https://www.instagram.com/gofx_thailand/?hl=th" target="_bank"><i class="fab fa-instagram"></i></a>
              <a href="https://twitter.com/gofxthailand" target="_bank"><i class="fab fa-twitter"></i></a>
          </div>
          <div class="humberger__menu__contact">
              <ul>
                  <li><i class="fa fa-envelope"></i> support@gofx.com</li>
              </ul>
          </div>
      </div>
  <!-- Humberger End -->

  <!-- Header Section Begin -->
  <header class="header">
      <div class="header__top">
          <div class="container">
            <div class="row">
              <div class="col-lg-2 pd-logo">
                <a href="index"><img src="/images/logo.png" class="logo-main"></a>
              </div>
              <div class="col-lg-2 pd-logo">
                <img src="/images/header_1.png" class="header-reward-1">
              </div>
              <div class="col-lg-8">
                <nav class="navbar navbar-expand-sm navbar-expand-gust justify-content-end header-guest">
                  <?php if($email == 'gofxempty_user@gmail.com' ){ ?>
                    <ul class="navbar-nav guest-nav">
                      <li class="nav-item active">
                        <a class="nav-link" href="<?= _SIGNIN_LINK ?>"><?= _SIGNIN ?></a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" href="<?= _REGISTER_LINK ?>" target="_blank"><?= _REGISTER ?></a>
                      </li>
                    </ul>
                  <?php }else{ ?>
                    <ul class="navbar-nav">
                      <div class="pdlr-10">
                        <img src="images/point.gif" width="40" height="40" alt="" class="avatar rounded-circle">
                      </div>
                      <li class="nav-item">
                        <span class="navbar-text header_top_text">
                          <?= $n_format; ?>
                        </span>
                      </li>
                      <div class="pdlr-10">
                        <img src="<?= $avatar; ?>" width="40" height="40" alt="" class="avatar rounded-circle">
                      </div>
                      <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle user-name" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <?= $name; ?>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                          <a class="dropdown-item" href="profile"><?= _PROFILE ?></a>
                          <a class="dropdown-item" href="logout"><?= _SIGNOUT ?></a>
                        </div>
                      </li>
                    </ul>
                  <?php } ?>
                  <!-- <div class="pdlr-10">
                    <img src="<?php echo $_ENV['APP_STORAGE']; ?>/images/flag/<?= _Flag ?>" width="30" height="30" alt="" class="">
                  </div>
                  <form method='get' action='' id='form_lang_desktop' >
                    <select name='lang' onchange='changeLang_desktop();' class="switch-lang header_top_text">
                    <option value='th' <?php if(isset($_SESSION['lang']) && $_SESSION['lang'] == 'th'){ echo "selected"; } ?> >TH</option>
                    <option value='en' <?php if(isset($_SESSION['lang']) && $_SESSION['lang'] == 'en'){ echo "selected"; } ?> >EN</option>
                   </select>
                  </form> -->

                </nav>
              </div>
            </div>
          </div>
      </div>
      <div class="container-fulid mgt-4">
        <div class="set-bg">
           <div class="container">
              <div class="col-lg-12">
                  <nav id="navbar_top" class="header__menu">
                      <ul>
                          <li class="<?php if($page == 'index') {echo 'active';}?>"><a href="index"><?= _MAINMENU_INDEX ?></a></li>
                          <li class="<?php if($page == 'rewards' || $page == 'reward_detail') {echo 'active';}?>"><a href="rewards"><?= _MAINMENU_REWARDS ?></a></li>
                          <li class="<?php if($page == 'history') {echo 'active';}?>"><a href="history"><?= _MAINMENU_HISTORY ?></a></li>
                          <li class="<?php if($page == 'term-and-conditions') {echo 'active';}?>"><a href="term-and-conditions"><?= _MAINMENU_TERM ?></a></li>
                          <?php if($email != 'gofxempty_user@gmail.com' ){ ?>
                          <li class="<?php if($page == 'profile') {echo 'active';}?>"><a href="profile"><?= _PROFILE ?></a></li>
                          <?php } ?>
                          <li class="<?php if($page == 'contact') {echo 'active';}?>"><a href="contact"><?= _MAINMENU_CONTACT ?></a></li>
                      </ul>
                  </nav>
              </div>
            </div>
            <?php if($page == 'index'){ ?>
              <!-- desktop , tablet version -->
              <div class="index-content container mb-lan-none">
                <div class="row mgrl-0 mgt-3 justify-content-end first_blog">
                   <div class="col-6 tab-var-none tab-var-none">
                   </div>
                   <div class="col-lg-6 col-md-6 col-12 tav-var-col-6 banner-box banner-header-text header-text-backgroud">
                     <h1 class="banner-header"><?= _BANNER_HEADTEXT1 ?></h1>
                     <p><?= _BANNER_TEXT1 ?></p>
                   </div>
                 </div>
                 <div class="row mgrl-0 mgt-1 justify-content-end">
                    <div class="col-6 mb-ver-none tab-var-none">
                    </div>
                    <div class="col-lg-6 col-md-6 col-12 tav-var-col-6 banner-box banner-point header-text-backgroud mb-lan-mgt2">
                      <div class="row banner-span banner-status pdtb-1 text-center">
                        <div class="col-5">
                          <span><?= $level_title ?></span>
                          <img src="<?php echo $_ENV['APP_STORAGE']; ?>/<?= $level_img ?>" class="img-fluid">
                        </div>
                        <div class="col-7 mg-none">
                          <span><?= _BANNER_HEADTEXT3 ?></span>
                          <h1 class="point-text"><?= $n_format; ?></h1>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row mgrl-0 mgt-1 mgb-15 justify-content-end ">
                     <div class="col-6 tab-var-none tab-var-none ">
                     </div>
                     <div class="col-lg-6 col-md-6 col-12 tav-var-col-6 banner-point header-text-backgroud">
                       <div class="row">
                         <div class="col-lg-6 col-md-7 col-12">
                           <p><?= _BANNER_TEXT3 ?></p>
                         </div>
                         <div class="col-lg-6 col-md-5 col-12">
                           <p class="index-num-text" onclick="return false"><?= number_format($point, 4, '.', ',')?></p>
                         </div>
                       </div>
                       <div class="row">
                         <div class="col-lg-6 col-md-7 col-12">
                           <p><?= _BANNER_TEXT4 ?></p>
                         </div>
                         <div class="col-lg-6 col-md-5 col-12">
                           <p class="index-num-text" onclick="return false"><?= number_format($lots, 4, '.', ',')?></p>
                         </div>
                       </div>
                     </div>
                  </div>
                </div>
                <!-- mobile version -->
                <div class="index-content container mb-ver mb-lan">
                  <div class="row mgrl-0 mgt-3 justify-content-end first_blog">
                     <div class="col-6  mb-lan-mgt2">
                       <div class="row banner-span banner-status pdtb-1 text-center">
                         <div class="col-12">
                           <span><?= $level_title ?></span>
                           <img src="<?php echo $_ENV['APP_STORAGE']; ?>/<?= $level_img ?>" class="img-fluid">
                         </div>
                         <div class="col-12 mg-none">
                           <span><?= _BANNER_MB_HEADTEXT3 ?></span>
                           <h1 class="point-text"><?= $n_format; ?></h1>
                         </div>
                       </div>
                       <div class="row text-center mgb-1">
                         <div class="col-12 pdlr-0">
                           <p class="index-num-text" onclick="return false"><?= _BANNER_MB_TEXT3 ?><?= number_format($point, 4, '.', ',')?></p>
                           <p class="index-num-text" onclick="return false"><?= _BANNER_MB_TEXT4 ?><?= number_format($lots, 4, '.', ',')?></p>
                         </div>
                       </div>
                     </div>
                   </div>
                   <div class="row mgrl-0 mgt-1 justify-content-end">
                     <div class="col-12 banner-header-text">
                       <h1 class="banner-header"><?= _BANNER_HEADTEXT1 ?></h1>
                       <p><?= _BANNER_MB_TEXT1 ?></p>
                     </div>
                   </div>
                </div>
              <?php } if($page == 'rewards'){ ?>
                <div class="container">
                  <div class="row reward-mg10 justify-content-center reward-level-img mb-ver-none tab-var-none mb-lan-none" id="nav-tab-desktop-ver">
                      <div class="icon-tab col active" id="go">
                          <img src="<?php echo $_ENV['APP_STORAGE']; ?>/images/status/20240109_073426_go_trader.gif" class="img-fulid reward-level-img-tab">
                          <div class="icon-tab-status mb-lan-tab-status">
                          <span class="icon-label"><?= _REWARD_RANK_1 ?></span>
                          </div>
                      </div>
                      <div class="icon-tab col col-md" id="advance">
                          <img src="<?php echo $_ENV['APP_STORAGE']; ?>/images/status/20240109_073414_advance_trader.gif" class="img-fulid reward-level-img-tab">
                          <div class="icon-tab-status">
                          <span class="icon-label"><?= _REWARD_RANK_2 ?></span>
                          </div>
                      </div>
                      <div class="icon-tab col col-md" id="expert">
                          <img src="<?php echo $_ENV['APP_STORAGE']; ?>/images/status/20240109_073359_expert_trader.gif" class="img-fulid reward-level-img-tab">
                          <div class="icon-tab-status">
                          <span class="icon-label"><?= _REWARD_RANK_3 ?></span>
                          </div>
                      </div>
                      <div class="icon-tab col col-md" id="master">
                          <img src="<?php echo $_ENV['APP_STORAGE']; ?>/images/status/20240109_065042_new_master_trader.gif" class="img-fulid reward-level-img-tab">
                          <div class="icon-tab-status">
                          <span class="icon-label"><?= _REWARD_RANK_4 ?></span>
                          </div>
                      </div>
                  </div>
                </div>
              <?php } ?>

        </div>
      </div>
  </header>
  <!-- Header Section End -->
