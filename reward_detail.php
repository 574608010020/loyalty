<?php $page = 'reward_detail'; include('template/header_temp.php'); ?>
<? (isset($_SESSION['lang']) ? $lang = $_SESSION['lang'] : $lang = 'th'); ?>
<? (isset($_COOKIE["user_id"]) ? $cookie_userid = $_COOKIE["user_id"] : $cookie_userid = ''); ?>
<style>
  .form-check-label {
    cursor: pointer;
  }
</style>
<div class="container mgtb-3">
  <?php
    $level_id = $_COOKIE["Level"];
    $GetLevel = new RewardDetail();
    $objGetLevel = $GetLevel->fncGetLevel($level_id);
    $arr = explode(" ",  $objGetLevel['level_title'] , 2);
  ?>
  <h3 class="reward-detail-title-1 text-center mb-ver-none"><?= _REWARD_DETAIL_TEXT_1 ?> <?= $objGetLevel['level_title']; ?> <span>: <?= $objGetLevel['rewards_level_title']; ?></span></h3>
  <h3 class="reward-detail-title-1 text-center mb-ver"><?= $objGetLevel['level_title']; ?> <span></br> <?= $objGetLevel['rewards_level_title']; ?></span></h3>
  <div class="row  mgt-2 mobile-version-detail">
    <div class="col-lg-12 col-md-12 col-12 pd-reward-detail"></div>
    <div class="col-lg-12 col-md-12 col-12 bg-reward-detail-items-1">
        <?php
          $GetDetail = new RewardDetail();
          $objGetDetail = $GetDetail->fncGetDetail($level_id,$email,$lang);
          $numResult = mysqli_num_rows($objGetDetail);
          echo ($numResult == 1 ? '<div class="row reward-level-img one-item">' : '<div class="row justify-content-center reward-level-img">');
        ?>
        <?php if($level_id != 57){ ?>
        <div class="reward-detail gofx-icon-tab col-2">
            <img src="images/GOFX_Loyalty.png" class="img-fulid">
        </div>
        <?php } ?> 
        <?php
          $i = 1;
          while( $row = mysqli_fetch_array($objGetDetail) ) {
            $json_img = json_decode($row['reward_items_img']);
            $img = $json_img->img0;
        ?>
          <div class="icon-tab  reward-detail col-2 <?= ($i == 1 ? "active":""); ?>">
              <img src="<?php echo $_ENV['APP_STORAGE']; ?>/<?= $img; ?>" class="img-fulid">
              <div class="icon-tab-status reward-icon-tab-status tab-var-none mb-ver-none mb-lan-none">
              <span class="icon-label"><?= "$row[reward_items_title]" ?></span>
              </div>
          </div>
        <?php
          $i++; }
        ?>
      </div>
    </div>
    <?php
      $num = 1;
      $slide = 0;
      foreach($objGetDetail as $item){
        $json_img = json_decode($item['reward_items_img']);
        $data_img = $json_img->img0;
        $rw_point = $item['rewards_level_point'];
    ?>
    <div class="item reward-detail col-lg-12">
        <div class="panel panel-default">
          <div class="row bg-reward-detail-items-2 pdtb-3">
            <div class="col-lg-6 col-md-12 col-12">
              <div id="carouselExampleIndicators<?= $num; ?>" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                  <?php
                    $slide = 0;
                    foreach($json_img as $img){
                  ?>
                  <li data-target="#carouselExampleIndicators<?= $num; ?>" data-slide-to="<?= $slide; ?>" class="<?= ($slide == 0 ? "active":""); ?>"></li>
                  <?php
                    $slide++; }
                  ?>
                </ol>
                <div class="carousel-inner">
                  <?php
                    $y = 1;
                    foreach($json_img as $img){
                  ?>
                  <div class="carousel-item <?= ($y == 1 ? "active":""); ?>">
                    <img class="d-block w-100" src="<?php echo $_ENV['APP_STORAGE']; ?>/<?= $img; ?>" alt="First slide">
                  </div>
                  <?php
                    $y++; }
                  ?>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators<?= $num; ?>" role="button" data-slide="prev">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators<?= $num; ?>" role="button" data-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
                </a>
              </div>
            </div>
            <div class="col-lg-6 col-md-12 col-12 reward-detail-desc-box">
              <h4 class="reward-detail-title-2"><?= $item['reward_items_title'] ?></h4>
              <p class="reward-detail-desc"><?= $item['reward_items_description'] ?></p>
              <h4 class="reward-detail-title-3"><?= _REWARD_DETAIL_TEXT_2 ?></h4>
              <p class="reward-detail-point"><img src="<?php echo $_ENV['APP_STORAGE']; ?>/images/point.png" class="img-fulid reward-detail-point-img"><?= number_format($item['rewards_level_point']),' ', _REWARD_TEXT_3 ?></p>
              <div class="row">
                <div class="col-12">
                  <?= ($item['receive'] != 1 && $email != 'gofxempty_user@gmail.com' ? '<p class="reward-detail-point-text text-center">'._REWARD_DETAIL_TEXT_3.'</p>':''); ?>
                  <?= ($item['history'] == 1 && $email != 'gofxempty_user@gmail.com' ? '<p class="reward-detail-point-text text-center">'._REWARD_DETAIL_TEXT_4.'</p>':'');?>
                  <?= ($item['receive'] == 1 && $item['history'] != 1 && $email != 'gofxempty_user@gmail.com' ? '<p class="reward-detail-point-text text-center">'._REWARD_DETAIL_TEXT_5.'</p>':''); ?>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 mb-ver-none">
                  <button class="btn btn-secondary btn-block btn-lg tb-btn" onclick="RedirectToReward('<?= strtolower($arr[0]) ?>')"><?= _REWARD_BUTTON_TEXT_1 ?></button>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6">
                  <?php if($email == 'gofxempty_user@gmail.com' ){ ?>
                    <button class="btn btn-danger btn-block btn-lg tb-btn view-item-trigger-clicked view-item" data-user-id="<?= $item['user_id']; ?>" data-item-id="<?= $item['id'] ?>" data-reward-title="<?= $item['reward_items_title'] ?>"
                    data-reward-point="<?= $item['rewards_level_point'] ?>" data-reward-img="<?= $data_img ?>" data-level-title="<?= $item['level_title'] ?>"
                    data-reward-level-title="<?= $item['rewards_level_title'] ?>" data-level-img="<?= $item['level_image'] ?>"
                    data-reward-detail="<?= $item['reward_items_description'] ?>" id="view-item">
                      <?= _REWARD_BUTTON_TEXT_2 ?>
                    </button>
                  <?php }else{ ?>
                  <button class="btn btn-danger btn-block btn-lg tb-btn view-item-trigger-clicked view-item"  id="view-item"
                  onclick="UserId('<?= $item['user_id']; ?>')" <?= ($item['receive'] != 1 ? 'disabled':'');?>  <?= ($item['history'] == 1 ? 'disabled':'');?>
                  data-user-id="<?= $item['user_id']; ?>" data-item-id="<?= $item['id'] ?>" data-reward-title="<?= $item['reward_items_title'] ?>"
                  data-reward-point="<?= $item['rewards_level_point'] ?>" data-reward-img="<?= $data_img ?>" data-level-title="<?= $item['level_title'] ?>"
                  data-reward-level-title="<?= $item['rewards_level_title'] ?>" data-level-img="<?= $item['level_image'] ?>"
                  data-reward-detail="<?= $item['reward_items_description'] ?>">
                    <?= _REWARD_BUTTON_TEXT_2 ?>
                  <?php } ?>
                  </button>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 mb-ver">
                  <button class="btn btn-secondary btn-block btn-lg tb-btn mb-btn" onclick="RedirectToReward('<?= strtolower($arr[0]) ?>')"><?= _REWARD_BUTTON_TEXT_1 ?></button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <?php
        $num++ ;}
       ?>
  </div>

  <div class="loyalty-slider mgt-5 display-none">
    <h3><?= _LOYALTY_SLIDER ?></h3>
    <div id="loyalty-activity" class="carousel slide" data-ride="carousel">
      <ol class="carousel-indicators">
        <li data-target="#loyalty-activity" data-slide-to="0" class="active"></li>
        <li data-target="#loyalty-activity" data-slide-to="1"></li>
        <li data-target="#loyalty-activity" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner">
        <div class="carousel-item active">
          <img class="d-block w-100" src="<?php echo $_ENV['APP_STORAGE']; ?>/images/img/01.jpg" alt="First slide">
        </div>
        <div class="carousel-item">
          <img class="d-block w-100" src="<?php echo $_ENV['APP_STORAGE']; ?>/images/img/02.jpg" alt="Second slide">
        </div>
        <div class="carousel-item">
          <img class="d-block w-100" src="<?php echo $_ENV['APP_STORAGE']; ?>/images/img/03.jpg" alt="Third slide">
        </div>
      </div>
      <a class="carousel-control-prev" href="#loyalty-activity" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#loyalty-activity" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade mgt-2" id="exampleModalCenter" data-backdrop="static">
  <?php if($email == 'gofxempty_user@gmail.com' ){ ?>
    <div class="modal-dialog modal-dialog-centered modal-md modal-login-dialog" role="document" id="guest-modal">
      <div class="modal-content">
        <div class="modal-header">
        </div>
        <div class="modal-body">
          <div class="container pdall-2">
            <div class="row">
              <div class="col-lg-12 col-md-12 col-12">
                <button type="button btn-success-close" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true" class="modal-cancel"><i class="fa fa-times" aria-hidden="true"></i></span>
                </button>
                <p class="login-text-1 mb-lan-none "><?= _REWARD_LOGIN_TEXT_1 ?></p>
                <p class="login-text-1 mb-ver"><?= _REWARD_LOGIN_TEXT_5 ?></p>
                <p class="login-text-2"><?= _REWARD_LOGIN_TEXT_2 ?></p>
                <div class="text-center mgb-1">
                  <img src="images/login.png" class="img-fulid reward-detail-login-img">
                </div>
                <p class="success-text-4"><?= _REWARD_LOGIN_TEXT_3 ?></p>
                <button type="button" class="btn btn-danger btn-lg success-text-5 text-center" data-dismiss="modal" onclick="window.location.href='<?=  _SIGNIN_LINK ?>'"><?= _REWARD_LOGIN_TEXT_4 ?></button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  <?php }else{ ?>
    <div class="modal-dialog modal-dialog-centered modal-md modal-login-dialog" role="document" id="address-modal">
      <div class="modal-content">
        <div class="modal-header">
        </div>
        <div class="modal-body">
          <div class="container pdall-2">
            <div class="row">
              <div class="col-lg-12 col-md-12 col-12">
                <button type="button btn-success-close" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true" class="modal-cancel"><i class="fa fa-times" aria-hidden="true"></i></span>
                </button>
                <p class="login-text-1 mb-lan-none "><?= _REWARD_ADDRESS_TEXT_1 ?></p>
                <p class="login-text-2"><?= _REWARD_ADDRESS_TEXT_2 ?></p>
                <div class="text-center mgb-1">
                  <img src="images/address.png" class="img-fulid reward-detail-login-img">
                </div>
                <p class="success-text-4"><?= _REWARD_ADDRESS_TEXT_3 ?></p>
                <button type="button" class="btn btn-danger btn-lg success-text-5 text-center" data-dismiss="modal" onclick="RedirectToProfile('1')"><?= _REWARD_ADDRESS_TEXT_4 ?></button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  <div class="modal-dialog modal-dialog-centered modal-confirm-dialog modal-lg" data-backdrop="static" role="document">
    <div class="modal-content">
      <div class="modal-header">
      </div>
      <div class="modal-body modal-reward-body" id="award-section">
        <div class="row modal-reward-head">
          <div class="col-lg-11 col-10 modal-reward-head-left mb-ver-none">
            <h3 class="modal-reward-head"><?= _REWARD_MODAL_TEXT_1 ?></h3>
            <h3 class="modal-reward-head-2"><span id="modal-view-level-title"></span> : <span id="modal-view-reward-level-title"></span></h3>
          </div>
          <div class="col-lg-11 col-10 modal-reward-head-left mb-ver">
            <h3 class="modal-reward-head"><?= _REWARD_MODAL_TEXT_2 ?></h3>
          </div>
          <div class="col-lg-1 col-2">
            <button type="button" class="close btn-close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true" class="modal-cancel"><i class="fa fa-times" aria-hidden="true"></i></span>
            </button>
          </div>
          <div class="col-12 modal-reward-head-mb mb-ver">
            <h3 class="modal-reward-head-2"><span id="modal-view-level-title-mb"></span> : <span id="modal-view-reward-level-title-mb"></span></h3>
          </div>
        </div>
        <div class="container pdall-2">
          <div class="row">
            <div class="col-lg-6 col-md-6 col-12">
              <img class="d-block modal-reward-img" src="" id="modal-view-reward-img" alt="First slide">
            </div>
            <div class="col-lg-6 col-md-6 col-12 modal-right-detail mgb-1">
              <h3 class="modal-reward-title-1" id="modal-view-reward-title"></h3>
              <p class="modal-detail-text" id="modal-view-reward-detail"></p>

              <form id="receive" method="post" class="mgb-1">
                <?php if($level_id == 57){ ?>
                <div id="option-size">
                  <div class="col-lg-6 col-lg-12">
                    <div class="form-check form-check-inline">
                      <input class="form-check-input" type="radio" name="size" id="s-size" value='S' required>
                      <label class="form-check-label" id="for-s" for="s-size">S - 36"x26.5"</label>
                    </div>
                    <div class="form-check form-check-inline">
                      <input class="form-check-input" type="radio" name="size" id="m-size" value='M'>
                      <label class="form-check-label" id="for-m" for="m-size">M - 38"x27.5"</label>
                    </div>
                  </div>
                  <div class="col-lg-6 col-lg-12">
                    <div class="form-check form-check-inline">
                      <input class="form-check-input" type="radio" name="size" id="l-size" value='L'>
                      <label class="form-check-label" id="for-l" for="l-size">L - 40"x28.5"</label>
                    </div>
                    <div class="form-check form-check-inline">
                      <input class="form-check-input" type="radio" name="size" id="xl-size" value='XL'>
                      <label class="form-check-label" id="for-xl" for="xl-size">XL - 42"x29.5"</label>
                    </div>
                  </div>
                </div>
              <?php } ?>
              <h4 class="modal-reward-title-2" id="">ที่อยู่ในการรับของรางวัล</h4>
              <?php
                $GetAddress = new UserAddress();
                $objGetAddress = $GetAddress->fncGetAddress($email,$lang);
                $address =  $objGetAddress['user_add'];
               ?>
              <p class="modal-detail-text" id=""><?= $address ?></p>
              <h4 class="modal-reward-title-2"><?= _REWARD_MODAL_TEXT_3 ?></h4>
              <p class="modal-detail-point mgb-13"><img src="<?php echo $_ENV['APP_STORAGE']; ?>/images/point.png" class="img-fulid reward-detail-point-img"><span id="modal-view-reward-point"></span> <?= _REWARD_MODAL_TEXT_4 ?></p>

                <input type="hidden" id="modal-view-user-id" name="user_id">
                <input type="hidden" id="modal-view-item-id" name="reward_items_id">
                <input type="hidden" id="modal-view-item-name" name="reward_items_name">
                <input type="hidden" id="modal-view-img" name="reward_items_image">
                <input type="hidden" name="email" value="<?= $email ?>">
                <input type="hidden" name="name" value="<?= $name ?>">
                <div class="modal-btn">
                  <button type="button" class="btn btn-light btn-lg modal-btn-cancel btn-reward-detail  mb-ver-none" data-dismiss="modal"><?= _REWARD_MODAL_TEXT_5 ?></button>
                  <button type="submit" name="receive" class="btn btn-danger btn-lg modal-btn-confirm btn-reward-detail" data-toggle="modal"><?= _REWARD_MODAL_TEXT_6 ?></button>
                </div>
                <div class="mb-ver mgt-1">
                <button type="button" class="btn btn-light btn-lg modal-btn-cancel btn-reward-detail" data-dismiss="modal"><?= _REWARD_MODAL_TEXT_5 ?></button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php } ?>
</div>

<div class="modal fade" id="exampleModalSuccess" tabindex="-1" role="dialog" data-backdrop="static" aria-labelledby="exampleModalSuccessTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-md modal-confirm-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
      </div>
      <div class="modal-body">
        <div class="container pdall-2">
          <div class="row">
            <div class="col-lg-12 col-md-12 col-12">
              <button type="button btn-success-close" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true" class="modal-cancel"><i class="fa fa-times" aria-hidden="true"></i></span>
              </button>
              <p class="success-text-1"><?= _REWARD_SUCCESS_TEXT_1 ?></p>
              <p class="success-text-2"><?= _REWARD_SUCCESS_TEXT_2 ?></p>
              <p class="success-text-3 mb-lan-none"><?= _REWARD_SUCCESS_TEXT_3 ?></p>
              <div class="text-center mgb-3">
                <img src="images/messageImage_1622529208597.jpg" class="img-fulid reward-detail-success-img">
              </div>
              <p class="success-text-4 mb-lan-none"><?= _REWARD_SUCCESS_TEXT_4 ?></p>
              <p class="success-text-4 mb-lan mb-ver"><?= _REWARD_SUCCESS_TEXT_6 ?></p>
                <div class="row">
                  <div class="col-12 mgb-1 mb-ver">
                    <button type="button" class="btn btn-danger btn-lg success-text-5 btn-modal-success text-center" data-dismiss="modal"><?= _REWARD_SUCCESS_TEXT_5 ?></button>
                  </div>
                  <div class="col-lg-6 col-md-6 col-12">
                    <button type="button" class="btn btn-light btn-lg success-text-5 btn-modal-success text-center" onclick="window.location.href='history'" data-dismiss="modal"><?= _REWARD_SUCCESS_TEXT_7 ?></button>
                  </div>
                  <div class="col-lg-6 col-md-6 col-12 mb-ver-none">
                    <button type="button" class="btn btn-danger btn-lg success-text-5 btn-modal-success text-center" data-dismiss="modal"><?= _REWARD_SUCCESS_TEXT_5 ?></button>
                  </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php include('template/footer_temp.php') ?>
<script type="text/javascript">

  function RedirectToReward(id_active){
    document.cookie = "id_active="+ id_active;
    window.location = 'rewards';
  }

  function RedirectToProfile(status){
    localStorage.setItem('modal_status', status);
    window.location = 'profile';
  }

  function UserId(user_id){
    document.cookie = "user_id="+ user_id;
  }


  $(document).on('click', ".view-item", function() {
    var chk_point = '<?=$rw_check_point?>';
    var rw_point = '<?=$rw_point?>';
    if(parseInt(rw_point) > parseInt(chk_point)){
      $(this).attr("disabled",true);
      return
    }
    
    var el = $(this);
    // get the data
    var user_id = el.attr('data-user-id');
    var item_id = el.attr('data-item-id');
    var reward_title = el.attr('data-reward-title');
    var reward_point = el.attr('data-reward-point');
    var reward_img = el.attr('data-reward-img');
    var level_title = el.attr('data-level-title');
    var reward_level_title = el.attr('data-reward-level-title');
    var level_img = el.attr('data-level-img');
    var reward_detail = el.attr('data-reward-detail');
    function formatNumber(num) {
      return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
    }

    if(item_id == 168 || item_id == 169){
      document.getElementById("s-size").disabled = false;
      document.getElementById("m-size").disabled = true;  
      document.getElementById("l-size").disabled = true;  
      document.getElementById("xl-size").disabled = true;  
      $("#for-s").addClass("mr-5");
      document.getElementById("for-m").innerHTML = 'M - 38"x27.5" (หมด)';
      document.getElementById("for-l").innerHTML = 'L - 40"x28.5" (หมด)';
      document.getElementById("for-xl").innerHTML = 'XL - 42"x29.5" (หมด)';
    }

    if(item_id == 180){
      document.getElementById("s-size").disabled = false;
      document.getElementById("m-size").disabled = false;  
      document.getElementById("l-size").disabled = false; 
      document.getElementById("xl-size").disabled = true;  
      $("#for-s").removeClass("mr-5");
      document.getElementById("for-m").innerHTML = 'M - 38"x27.5"';
      document.getElementById("for-l").innerHTML = 'L - 40"x28.5"';
      document.getElementById("for-xl").innerHTML = 'XL - 42"x29.5" (หมด)';
    }


    if(item_id == 181){
      document.getElementById("s-size").disabled = false;
      document.getElementById("m-size").disabled = false;  
      document.getElementById("l-size").disabled = false; 
      document.getElementById("xl-size").disabled = true;  
      $("#for-s").removeClass("mr-5");
      document.getElementById("for-m").innerHTML = 'M - 38"x27.5"';
      document.getElementById("for-l").innerHTML = 'L - 40"x28.5"';
      document.getElementById("for-xl").innerHTML = 'XL - 42"x29.5" (หมด)';
    }

    var point = formatNumber(reward_point);
    // alert(item_id);
    $("#modal-view-user-id").val(user_id);
    $("#modal-view-item-id").val(item_id);
    $("#modal-view-item-name").val(reward_title);
    $("#modal-view-img").val('<?php echo $_ENV['APP_STORAGE']; ?>/'+reward_img);
    $("#modal-view-reward-title").text(reward_title);
    $("#modal-view-reward-point").text(point);
    $("#modal-view-reward-img").attr('src','<?php echo $_ENV['APP_STORAGE']; ?>/'+reward_img);
    $("#modal-view-reward-level-title").text(reward_level_title);
    $("#modal-view-reward-level-title-mb").text(reward_level_title);
    $("#modal-view-level-title").text(level_title);
    $("#modal-view-level-title-mb").text(level_title);
    $("#modal-view-level-img").attr('src','<?php echo $_ENV['APP_STORAGE']; ?>/'+level_img);
    $("#modal-view-reward-detail").text(reward_detail);
    $('#exampleModalCenter').modal('show');
    $.ajax({
      url: '/func/history/checkaddress.php',
      method: 'get',
      data: {user_id:user_id},
      success: function(data) {
        if(data != 0){
          $('#award-section').show();
          $('#address-modal').hide();
        }
        else{
          $('#award-section').hide();
          $('#address-modal').show();
        }
      }
    });
  });

  // RECEIVE
  $('#receive').on('submit', function(e) {
    e.preventDefault();

    $.ajax({
      url: '/func/history/check.php', 
      method: 'post',
      data: new FormData(this),
      cache: false,
      contentType: false,
      processData: false,
      dataType: 'json',
      success: function(data) {
        if (!data.history) {
          $('#exampleModalCenter').modal('hide');
          $('#exampleModalSuccess').modal('show');
          $('.view-item').attr('disabled', true);
        }
      }
    });
  })


  $('#exampleModalSuccess').on('hidden.bs.modal', function () {
    localStorage.removeItem('openModal');
  })
</script>
