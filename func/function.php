<?php

include("conn.php");

/********** ข้อมูลสถานะ level **********/
class Level extends DB_con {
  function fncGetLevelDetail(){
    $sql = "SELECT * FROM level";
    $result =  mysqli_query( $this->dbcon,$sql);
    return $result;
  }
}

/********** query หน้าแรก **********/
class Index extends DB_con {
  function fncGetLevel($email){
    $sql = "SELECT user.*, level.*,
            CASE WHEN user.level_id >= level.id THEN 1 ELSE 0 END AS reward
            FROM
            (SELECT email, level_id
             FROM users
             WHERE email = '$email') user
            CROSS JOIN
            (SELECT * FROM level) level";
    $result =  mysqli_query( $this->dbcon,$sql);
    return $result;
  }

  function fncCntGetLevel($email){
    $sql = "SELECT COUNT(*) AS cnt FROM
            (SELECT user.*, level.*,
                        CASE WHEN user.level_id >= level.id THEN 1 ELSE 0 END AS reward
                        FROM
                        (SELECT email, level_id
                         FROM users
                         WHERE email = '$email') user
                        CROSS JOIN
                        (SELECT * FROM level) level ) cnt
            WHERE cnt.reward = 1 ";
    $result_chk = $this->dbcon->query($sql);
    $result = $result_chk->fetch_assoc();
    return $result;
  }
}

/********** query check ผู้ใช้งาน **********/
class CheckUser extends DB_con {
  function fncCheckUser($email) {
    $sql = "SELECT a.*, b.*
            FROM users a
            LEFT JOIN level b ON a.level_id = b.id
            WHERE email = '$email'";
    $result_chk = $this->dbcon->query($sql);
    $result = $result_chk->fetch_assoc();
    return $result;
  }

  function fncCntUser($email) {
    $sql = "SELECT COUNT(*) AS cnt FROM
            (SELECT *
            FROM users
            WHERE email = '$email') cnt";
    $result_chk = $this->dbcon->query($sql);
    $result = $result_chk->fetch_assoc();
    return $result;
  }

  function fncInsertUser($email) {
    $sql = "INSERT INTO origin_lots (email,lots) VALUES ('$email','0')";
    $query = mysqli_query( $this->dbcon,$sql);
  }
}

/********** query ที่อยู่ลูกค้า***********/
class UserAddress extends DB_con {
  function fncGetAddress($email,$lang) {
    $sql = "SELECT a.address, a.tel, c.id as province_id, c.name_$lang, d.id as amphure_id, d.name_$lang, e.id as district_id, e.name_$lang, a.zipcode,
            CONCAT(a.address,' ',e.name_$lang,' ',d.name_$lang,' ',c.name_$lang,' ',a.zipcode) AS user_add
            FROM loyalty.user_address a
            LEFT JOIN loyalty.users b ON a.user_id = b.id
            LEFT JOIN region_db.provinces c ON a.province = c.id
            LEFT JOIN region_db.amphures d ON d.province_id = c.id
            LEFT JOIN region_db.districts e ON e.amphure_id = d.id
            WHERE b.email = '$email' and e.id = a.district";
    $result_chk = $this->dbcon->query($sql);
    $result = $result_chk->fetch_assoc();
    return $result;
  }
  function fncInsertAddress($fields,$values) {
    $sql = "INSERT INTO user_address ($fields)
           VALUES ($values)";
    $query = mysqli_query( $this->dbcon,$sql);
  }
  function fncCheckAddress($email) {
    $sql = "SELECT COUNT(*) AS cnt FROM
            (SELECT a.*
            FROM user_address a
            LEFT JOIN users b ON a.user_id = b.id
            WHERE b.email = '$email'
            ) cnt";
    $result_chk = $this->dbcon->query($sql);
    $result = $result_chk->fetch_assoc();
    return $result;
  }
}

/********** query check ของรางวัลพิเศษ **********/
class CheckSpecialItem extends DB_con {
  function fncCheckSpecialItem($email) {
    $sql = "SELECT COUNT(*) AS cnt FROM
            (SELECT a.*
            FROM special_item_history a
            LEFT JOIN users b ON a.user_id = b.id
            WHERE b.email = '$email'
            ) cnt";
    $result_chk = $this->dbcon->query($sql);
    $result = $result_chk->fetch_assoc();
    return $result;
  }
} 

/********** query หน้าประวัติการแลก **********/ 
class History extends DB_con {
  function hisAward($id, $user_email) {
    // $sql = "SELECT i.*, l.rewards_level_point, d.reward_items_title
    //         FROM reward_items i 
    //         INNER JOIN rewards_level l ON l.id = i.reward_level_id
    //         INNER JOIN reward_items_detail_$lang d ON d.reward_items_id = i.id
    //         WHERE i.id = $id";

    // $sql = "SELECT l.rewards_level_point, h.reward_items_name as reward_items_title, h.reward_items_image as reward_items_img
    //         FROM history h
    //         INNER JOIN reward_items r ON h.reward_items_id = r.id
    //         INNER JOIN rewards_level l ON l.id = r.reward_level_id
    //         INNER JOIN users u ON h.user_id = u.id
    //         WHERE u.email = '$user_email' AND h.reward_items_id = $id"; 
    $sql = "SELECT l.*, h.reward_items_id, rl.rewards_level_point, r.reward_items_img, rd.reward_items_title
            FROM `log`l
            INNER JOIN history h ON l.history_id = h.id
            LEFT JOIN users u ON u.id = l.user_id
            LEFT JOIN reward_items r ON h.reward_items_id = r.id
            LEFT JOIN rewards_level rl ON rl.id = r.reward_level_id
            LEFT JOIN reward_items_detail_th rd ON r.id = rd.reward_items_id
            where type = 'history' and email = '$user_email' and h.reward_items_id = $id";
    $result = mysqli_query($this->dbcon, $sql);

    return mysqli_fetch_assoc($result);
  }
  function fncHistory($email,$date,$lang) {
    $sql = "SELECT l.*
            FROM `log` l
            INNER JOIN origin_lots o ON o.id = l.user_id
            WHERE o.email = '$email' 
            AND DATE_FORMAT(l.created_at, '%Y-%m-%d') 
            BETWEEN CURDATE() - INTERVAL '$date' DAY 
            AND CURDATE()
            ORDER BY l.created_at DESC";
    $result = mysqli_query($this->dbcon, $sql);
    return $result;
  }
  function fncBetweenHistory($email,$start_date,$end_date,$lang) {
    $sql = "SELECT l.*
            FROM `log` l
            INNER JOIN origin_lots o ON o.id = l.user_id
            WHERE o.email = '$email' 
            AND DATE_FORMAT(l.created_at, '%Y-%m-%d') 
            BETWEEN '$start_date' AND '$end_date'
            ORDER BY l.created_at DESC";
    $result = mysqli_query($this->dbcon, $sql);
    return $result;
  }
  function fncCntHistory($email,$date,$lang){
    $sql = "SELECT COUNT(*) AS cnt FROM
            (SELECT l.*
              FROM `log` l
              INNER JOIN origin_lots o ON o.id = l.user_id
              WHERE o.email = '$email' 
              AND DATE_FORMAT(l.created_at, '%Y-%m-%d') 
              BETWEEN CURDATE() - INTERVAL '$date' DAY 
              AND CURDATE()
              ORDER BY l.created_at DESC
            ) cnt";
    $result_chk = $this->dbcon->query($sql);
    $result = $result_chk->fetch_assoc();
    return $result;
  }
  function fncCheckHistory($email){
    $sql = "SELECT COUNT(*) AS cnt FROM
            (SELECT a.*
            FROM history a
            LEFT JOIN users b ON a.user_id = b.id
            WHERE b.email = 'gofxtestibno3@gmail.com'
            ) cnt";
    $result_chk = $this->dbcon->query($sql);
    $result = $result_chk->fetch_assoc();
    return $result;
  }
}

/********** query หน้าแลกของรางวัล **********/
class Reward extends DB_con {
  /********** GO Trader **********/
  function fncGOTrader($email,$lang) {
  $sql = "SELECT user.email, user.timeline, reward.rewards_level_title, reward.rewards_level_short_description, reward.rewards_level_point, reward.rewards_level_image,reward.id,
            CASE WHEN user.timeline >= reward.rewards_level_point THEN 1 ELSE 0 END AS reward_status
            FROM
            (SELECT email, point, timeline FROM users WHERE email = '$email') user
            CROSS JOIN
            (SELECT a.*, c.rewards_level_short_description
            FROM rewards_level a
            LEFT JOIN reward_items b ON a.id = b.reward_level_id
            LEFT JOIN rewards_level_detail_$lang c ON a.id = c.rewards_level_id
            WHERE a.level_id = 1 AND a.rewards_level_status = 1
            GROUP BY a.id) reward
            ORDER BY reward.rewards_level_point ASC";
    $result = mysqli_query($this->dbcon, $sql);
    return $result;
  }

  function fncCntGOTrader($email){
    $sql = "SELECT COUNT(*) AS cnt FROM
            (SELECT user.email, user.timeline, reward.rewards_level_title, reward.rewards_level_point, reward.rewards_level_image,reward.id,
                        CASE WHEN user.timeline >= reward.rewards_level_point THEN 1 ELSE 0 END AS reward_status
                        FROM
                        (SELECT email, point, timeline FROM users WHERE email = '$email') user
                        CROSS JOIN
                        (SELECT a.*
                        FROM rewards_level a
                        LEFT JOIN reward_items b ON a.id = b.reward_level_id
                        WHERE a.level_id = 1 and a.rewards_level_status = 1
                        GROUP BY a.id) reward) cnt
            WHERE cnt.reward_status = 1";
    $result_chk = $this->dbcon->query($sql);
    $result = $result_chk->fetch_assoc();
    return $result;
  }

  function fncGOTraderHistory($email){
    $sql = "SELECT a.*, c.reward_items_id, c.user_id
             FROM rewards_level a
             LEFT JOIN reward_items b ON a.id = b.reward_level_id
             LEFT JOIN history c ON b.id = c.reward_items_id
             LEFT JOIN users d ON d.id = c.user_id
             WHERE a.level_id = 1 and d.email = '$email' AND c.history_status != 4
             GROUP BY a.id";
    $result = mysqli_query($this->dbcon, $sql);
    return $result;
  }

  function fncGOTraderSpecialHistory($email){
    $sql = "SELECT a.*, c.reward_items_id, c.user_id
             FROM rewards_level a
             LEFT JOIN reward_items b ON a.id = b.reward_level_id
             LEFT JOIN special_item_history c ON b.id = c.reward_items_id
             LEFT JOIN users d ON d.id = c.user_id
             WHERE a.level_id = 1 and d.email = '$email'
             GROUP BY a.id";
    $result = mysqli_query($this->dbcon, $sql);
    return $result;
  }

  /********** Advance Trader **********/
  function fncAdvanceTrader($email,$lang) {
    $sql = "SELECT user.email, user.timeline, reward.rewards_level_title, reward.rewards_level_short_description, reward.rewards_level_point, reward.rewards_level_image,reward.id,
            CASE WHEN user.timeline >= reward.rewards_level_point THEN 1 ELSE 0 END AS reward_status
            FROM
            (SELECT email, point, timeline FROM users WHERE email = '$email') user
            CROSS JOIN
            (SELECT a.*, c.rewards_level_short_description
            FROM rewards_level a
            LEFT JOIN reward_items b ON a.id = b.reward_level_id
            LEFT JOIN rewards_level_detail_$lang c ON a.id = c.rewards_level_id
            WHERE a.level_id = 2
            GROUP BY a.id) reward";
    $result = mysqli_query($this->dbcon, $sql);
    return $result;
  }

  function fncCntAdvanceTrader($email){
    $sql = "SELECT COUNT(*) AS cnt FROM
            (SELECT user.email, user.timeline, reward.rewards_level_title, reward.rewards_level_point, reward.rewards_level_image,reward.id,
                        CASE WHEN user.timeline >= reward.rewards_level_point THEN 1 ELSE 0 END AS reward_status
                        FROM
                        (SELECT email, point, timeline FROM users WHERE email = '$email') user
                        CROSS JOIN
                        (SELECT a.*
                        FROM rewards_level a
                        LEFT JOIN reward_items b ON a.id = b.reward_level_id
                        WHERE a.level_id = 2 and a.rewards_level_status = 1
                        GROUP BY a.id) reward) cnt
            WHERE cnt.reward_status = 1";
    $result_chk = $this->dbcon->query($sql);
    $result = $result_chk->fetch_assoc();
    return $result;
  }

  function fncAdvanceTradeHistory($email){
    $sql = "SELECT a.*, c.reward_items_id, c.user_id
             FROM rewards_level a
             LEFT JOIN reward_items b ON a.id = b.reward_level_id
             LEFT JOIN history c ON b.id = c.reward_items_id
             LEFT JOIN users d ON d.id = c.user_id
             WHERE a.level_id = 2 and d.email = '$email' AND c.history_status != 4
             GROUP BY a.id";
    $result = mysqli_query($this->dbcon, $sql);
    return $result;
  }

  /********** Expert Trader **********/
  function fncExpertTrader($email,$lang) {
    $sql = "SELECT user.email, user.timeline, reward.rewards_level_title, reward.rewards_level_short_description, reward.rewards_level_point, reward.rewards_level_image,reward.id,
            CASE WHEN user.timeline >= reward.rewards_level_point THEN 1 ELSE 0 END AS reward_status
            FROM
            (SELECT email, point, timeline FROM users WHERE email = '$email') user
            CROSS JOIN
            (SELECT a.*, c.rewards_level_short_description
            FROM rewards_level a
            LEFT JOIN reward_items b ON a.id = b.reward_level_id
            LEFT JOIN rewards_level_detail_$lang c ON a.id = c.rewards_level_id
            WHERE a.level_id = 3
            GROUP BY a.id) reward";
    $result = mysqli_query($this->dbcon, $sql);
    return $result;
  }

  function fncCntExpertTrader($email){
    $sql = "SELECT COUNT(*) AS cnt FROM
            (SELECT user.email, user.timeline, reward.rewards_level_title, reward.rewards_level_point, reward.rewards_level_image,reward.id,
                        CASE WHEN user.timeline >= reward.rewards_level_point THEN 1 ELSE 0 END AS reward_status
                        FROM
                        (SELECT email, point, timeline FROM users WHERE email = '$email') user
                        CROSS JOIN
                        (SELECT a.*
                        FROM rewards_level a
                        LEFT JOIN reward_items b ON a.id = b.reward_level_id
                        WHERE a.level_id = 3 and a.rewards_level_status = 1
                        GROUP BY a.id) reward) cnt
            WHERE cnt.reward_status = 1";
    $result_chk = $this->dbcon->query($sql);
    $result = $result_chk->fetch_assoc();
    return $result;
  }

  function fncExpertTradeHistory($email){
    $sql = "SELECT a.*, c.reward_items_id, c.user_id
             FROM rewards_level a
             LEFT JOIN reward_items b ON a.id = b.reward_level_id
             LEFT JOIN history c ON b.id = c.reward_items_id
             LEFT JOIN users d ON d.id = c.user_id
             WHERE a.level_id = 3 and d.email = '$email' AND c.history_status != 4
             GROUP BY a.id";
    $result = mysqli_query($this->dbcon, $sql);
    return $result;
  }

  /********** Master Trader lv.1-13 **********/
  function fncMasterTrader1($email,$lang) {
    $sql = "SELECT user.email, user.timeline, reward.rewards_level_title, reward.rewards_level_short_description, reward.rewards_level_point, reward.rewards_level_image,reward.id,
            CASE WHEN user.timeline >= reward.rewards_level_point THEN 1 ELSE 0 END AS reward_status
            FROM
            (SELECT email, point, timeline FROM users WHERE email = '$email') user
            CROSS JOIN
            (SELECT a.*, c.rewards_level_short_description
            FROM rewards_level a
            LEFT JOIN reward_items b ON a.id = b.reward_level_id
            LEFT JOIN rewards_level_detail_$lang c ON a.id = c.rewards_level_id
            WHERE a.level_id = 4
            GROUP BY a.id) reward
            LIMIT 0,13";
    $result = mysqli_query($this->dbcon, $sql);
    return $result;
  }

  function fncCntMasterTrader1($email){
    $sql = "SELECT COUNT(*) AS cnt FROM
            (SELECT user.email, user.timeline, reward.rewards_level_title, reward.rewards_level_point, reward.rewards_level_image,reward.id,
                        CASE WHEN user.timeline >= reward.rewards_level_point THEN 1 ELSE 0 END AS reward_status
                        FROM
                        (SELECT email, point, timeline FROM users WHERE email = '$email') user
                        CROSS JOIN
                        (SELECT a.*
                        FROM rewards_level a
                        LEFT JOIN reward_items b ON a.id = b.reward_level_id
                        WHERE a.level_id = 4 and a.rewards_level_status = 1
                        GROUP BY a.id) reward
            LIMIT 0,13) cnt
            WHERE cnt.reward_status = 1";
    $result_chk = $this->dbcon->query($sql);
    $result = $result_chk->fetch_assoc();
    return $result;
  }

  /********** Master Trader lv.14-25 **********/
  function fncMasterTrader2($email,$lang) {
    $sql = "SELECT user.email, user.timeline, reward.rewards_level_title, reward.rewards_level_short_description, reward.rewards_level_point, reward.rewards_level_image,reward.id,
            CASE WHEN user.timeline >= reward.rewards_level_point THEN 1 ELSE 0 END AS reward_status
            FROM
            (SELECT email, point, timeline FROM users WHERE email = '$email') user
            CROSS JOIN
            (SELECT a.*, c.rewards_level_short_description
            FROM rewards_level a
            LEFT JOIN reward_items b ON a.id = b.reward_level_id
            LEFT JOIN rewards_level_detail_$lang c ON a.id = c.rewards_level_id
            WHERE a.level_id = 4
            GROUP BY a.id) reward
            LIMIT 13,25";
    $result = mysqli_query($this->dbcon, $sql);
    return $result;
  }

  function fncCntMasterTrader2($email){
    $sql = "SELECT COUNT(*) AS cnt FROM
            (SELECT user.email, user.timeline, reward.rewards_level_title, reward.rewards_level_point, reward.rewards_level_image,reward.id,
                        CASE WHEN user.timeline >= reward.rewards_level_point THEN 1 ELSE 0 END AS reward_status
                        FROM
                        (SELECT email, point, timeline FROM users WHERE email = '$email') user
                        CROSS JOIN
                        (SELECT a.*
                        FROM rewards_level a
                        LEFT JOIN reward_items b ON a.id = b.reward_level_id
                        WHERE a.level_id = 4 and a.rewards_level_status = 1
                        GROUP BY a.id) reward
            LIMIT 13,25) cnt
            WHERE cnt.reward_status = 1";
    $result_chk = $this->dbcon->query($sql);
    $result = $result_chk->fetch_assoc();
    return $result;
  }

  function fncMasterTradeHistory($email){
    $sql = "SELECT a.*, c.reward_items_id, c.user_id
             FROM rewards_level a
             LEFT JOIN reward_items b ON a.id = b.reward_level_id
             LEFT JOIN history c ON b.id = c.reward_items_id
             LEFT JOIN users d ON d.id = c.user_id
             WHERE a.level_id = 4 and d.email = '$email' AND c.history_status != 4
             GROUP BY a.id";
    $result = mysqli_query($this->dbcon, $sql);
    return $result;
  }
}
 
/********** query หน้ารายละเอียดของรางวัล **********/
class RewardDetail extends DB_con {
  function fncGetDetail($level_id,$email,$lang){
    $sql = "SELECT user.id as user_id, user.email, user.timeline, reward.id, reward.reward_level_id, reward.rewards_level_point,reward.reward_items_img,
            reward.reward_items_title, reward.reward_items_description, reward.level_title, reward.rewards_level_title,reward.level_image,
            CASE WHEN user.timeline >= reward.rewards_level_point THEN 1 ELSE 0 END receive,
            CASE WHEN (SELECT a.user_id
            FROM history a
            LEFT JOIN reward_items b ON a.reward_items_id = b.id
            LEFT JOIN rewards_level c ON b.reward_level_id = c.id
            LEFT JOIN users d ON a.user_id = d.id
            WHERE c.id = $level_id AND d.email = '$email' AND a.history_status != 4
            LIMIT 1) THEN 1 ELSE 0 END history
            FROM
            (SELECT id, email, point, timeline
             FROM users
             WHERE email = '$email') user
            CROSS JOIN
            (SELECT a.*, b.reward_items_title, b.reward_items_description,c.rewards_level_point, d.level_title, c.rewards_level_title, d.level_image
             FROM reward_items a
             LEFT JOIN reward_items_detail_$lang b ON a.id = b.reward_items_id
             LEFT JOIN rewards_level c ON a.reward_level_id = c.id
             LEFT JOIN level d ON c.level_id = d.id
             WHERE a.reward_level_id = $level_id and a.reward_status != 0) reward
            ";
   $result = mysqli_query($this->dbcon, $sql);
   return $result;
  }

  function fncGetLevel($level_id){
    $sql = "SELECT c.level_title, b.rewards_level_title
            FROM reward_items a
            LEFT JOIN rewards_level b ON a.reward_level_id = b.id
            LEFT JOIN level c ON b.level_id = c.id
            WHERE a.reward_level_id = $level_id
            LIMIT 1";
    $result_chk = $this->dbcon->query($sql);
    $result = $result_chk->fetch_assoc();
    return $result;
  }
}

/********** Line Notify **********/
class LineNotify extends DB_con {
  function fncLineNotify($item_id) {
   $sql = "SELECT a.reward_items_title, b.reward_items_img, d.level_title, c.rewards_level_title, c.rewards_level_point
          FROM reward_items_detail_th a
          LEFT JOIN reward_items b ON b.id = a.reward_items_id
          LEFT JOIN rewards_level c ON b.reward_level_id = c.id
          LEFT JOIN level d ON c.level_id = d.id
          WHERE a.reward_items_id = $item_id";
   $result_chk = $this->dbcon->query($sql);
   $result = $result_chk->fetch_assoc();
   return $result;
 }
}

/********** GET API **********/
class GetAPI extends DB_con{
  function readOne($email){

      // query to read single record
      $sql = "SELECT user.user_id, user.fullname, user.user_email, user.user_birthday, user.user_sex, SUM(user.VOLUME) AS lots
                FROM
                (SELECT ac.user_id, IF(CONCAT(sv.fname_en,' ',sv.lname_en) IS NULL, ac.user_fullname, CONCAT(sv.fname_en,' ',sv.lname_en)) AS fullname,
                 ac.user_email, ac.user_birthday, ac.user_sex, rp.LOGIN, IF(LOGIN LIKE '86%', SUM((rp.VOLUME/100)/100), SUM(rp.VOLUME/100)) AS VOLUME,
                 rp.OPEN_TIME
                 FROM account.bk_user_account ac
                 LEFT JOIN servicefx.interview_gofx sv ON ac.user_id = sv.user_id
                 LEFT JOIN mt4.treder_account mt ON mt.user_id = ac.user_id
                 LEFT JOIN reportserver.MT4_TRADES rp ON rp.LOGIN = mt.ta_login
                 WHERE ac.user_email = '$email' and rp.OPEN_TIME > '2021-03-01 00:00:00'
                 GROUP BY LOGIN) user
                 GROUP BY user.user_id";

      // prepare query statement
      $query = mysqli_query( $this->dbcon,$sql);

      // get retrieved row
      $row = $query->fetch_assoc();

      // set values to object properties
      $this->user_id = $row['user_id'];
      $this->fullname = $row['fullname'];
      $this->user_email = $row['user_email'];
      $this->user_birthday = $row['user_birthday'];
      $this->user_sex = $row['user_sex'];
      $this->lots = $row['lots'];
  }
}

/********** Daily Login **********/
class DailyLogin extends DB_con {
  function fncGetItems() {
   $sql = "SELECT * FROM promotion_rewards";
   $result = mysqli_query($this->dbcon, $sql);
   return $result;
  }

  function fncGetCheckDate($email) {
    $sql = "SELECT check_date, created_at, updated_at FROM promotion_checkin WHERE email = '$email'";
    $result = mysqli_query($this->dbcon, $sql);
    return $result;
  }

  function fncGetUserId($email) {
    $sql = "SELECT id FROM users WHERE email = '$email'";
    $result = mysqli_query($this->dbcon, $sql);
    return $result;
  }

}

/********** Daily Login **********/
class MT4Product extends DB_con {
  function fncGetMT4Product() {
   $sql = "SELECT DISTINCT * FROM `contract_size` ORDER BY `account_type`, id ASC";
   $result = mysqli_query($this->dbcon, $sql);
   return $result;
  }

  function fncGofx(){
    $sql = "SELECT a.email, a.check_date AS 'checkin1', a.created_at AS 'checkin2', a.updated_at AS 'checkin3', b.option, h.user_fullname, d.tel, CONCAT(d.address,' ',g.name_th,' ',f.name_th,' ',e.name_th,' ',d.zipcode) AS user_add
            FROM loyalty.promotion_checkin a
            LEFT JOIN loyalty.promotion_reward_options b ON a.email = b.email
            LEFT JOIN loyalty.users c ON a.email = c.email
            LEFT JOIN loyalty.user_address d ON d.user_id = c.id
            LEFT JOIN region_db.provinces e ON d.province = e.id
            LEFT JOIN region_db.amphures f ON f.province_id = e.id
            LEFT JOIN region_db.districts g ON g.amphure_id = f.id
            LEFT JOIN account.bk_user_account h ON a.email = h.user_email
            LEFT JOIN loyalty.promotion_users i ON i.email = a.email
            WHERE g.id = d.district and i.created_at != '2021-11-01 09:45:16'
            ORDER BY a.check_date DESC, updated_at ASC";
    $result = mysqli_query($this->dbcon, $sql);
    return $result;
  }

}
