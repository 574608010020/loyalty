<?php

include_once('DotENV.php');

use DevCoder\DotEnv;
(new DotEnv(__DIR__ . '/.env'))->load();

define('KEY', getenv('KEY'));

class Decrypt {
  public function decryptData($data) {
    // Remove the base64 encoding from our key
    $encryption_key = base64_decode(KEY);
    // To decrypt, split the encrypted data from our IV - our unique separator used was "::"
    list($encrypted_data, $iv) = explode('::', base64_decode($data), 2);
    return openssl_decrypt($encrypted_data, 'aes-256-cbc', $encryption_key, 0, $iv);
  }
}