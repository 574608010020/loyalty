<?php

include_once 'Query.php';

class Lot extends Query {

  public function store($json) {
    if (!empty($this->emailExist($json))) return $this->update($json);
    
    $sql = "INSERT INTO origin_lots (email, lots) VALUES ('$json->email', $json->lots)";
    
    return $this->query_data($sql);
  }

  public function update($json) {
    $sql = "UPDATE origin_lots SET lots = $json->lots WHERE email = '$json->email'";

    return $this->query_data($sql);
  }

  public function emailExist($json) {
    $sql = "SELECT * FROM origin_lots WHERE email = '$json->email'";
    
    return $this->select_assoc($sql);
  }
}