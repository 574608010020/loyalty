<?php

require_once __DIR__ . '/Query.php';

class Address extends Query {

  public function checkUserID($email) {
    $sql = "SELECT id as user_id FROM origin_lots WHERE email = '$email'";
    return $this->select_assoc($sql);
  }

  public function checkExist($email) {
    $sql = "SELECT u.id as user_id
            FROM user_address a
            LEFT JOIN origin_lots u ON u.id = a.user_id
            WHERE u.email = '$email'";
    return $this->select_assoc($sql);
  }

  public function index($email) {
    $user = $this->checkUserID($email);
    $sql = "SELECT * FROM user_address WHERE user_id = " . $user['user_id'];

    return $this->select_assoc($sql);
  }

  public function store($data) {
    $email = $data['email'];
    $user = $this->checkUserID($email);
    $data['user_id'] = $user['user_id'];  
    unset($data['email']);
    if (count($this->checkExist($email))) {
      $this->update($data, $user);
    }
    else {
      $fields = implode(',', array_keys($data));
      $values = "'" . implode("','", $data) . "'";
      $user_id = $data['user_id'];
      $address = $_POST['address'];
      $district = $_POST['district'];
      $amphure = $_POST['amphure'];
      $province = $_POST['province'];
      $zipcode = $_POST['zipcode'];
      $tel = $_POST['tel'];      
      $sql = "INSERT INTO user_address (user_id,address,district,amphure,province,zipcode,tel) 
              VALUES ('$user_id','$address','$district','$amphure','$province','$zipcode','$tel')";
      $this->query_data($sql);
    }
  }

  public function update($data, $user) {
    $user_id = $user['user_id'];
    $output = array();
    foreach ($data as $key => $val) {
      $output[] = "$key = '$val'";
    }
    $output = implode(",", $output);
    $address = $data['address'];
    $district = $data['district'];
    $amphure = $data['amphure'];
    $province = $data['province'];
    $zipcode = $data['zipcode'];
    $tel = $data['tel']; 
    $sql = "UPDATE user_address SET address = '$address',province = '$province',amphure = '$amphure',
            district = '$district',zipcode = '$zipcode',tel = '$tel' WHERE user_id = $user_id";
    $this->query_data($sql);
  }

}
