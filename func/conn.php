<?php

include_once('DotENV.php');

use DevCoder\DotEnv;
(new DotEnv(__DIR__ . '/.env'))->load();

define('DB_SERV', getenv('DB_SERV'));
define('DB_USER', getenv('DB_USER'));
define('DB_PASS', getenv('DB_PASS'));
define('DB_NAME', getenv('DB_NAME'));

class DB_con {

  function __construct() {
    $conn = mysqli_connect(DB_SERV, DB_USER, DB_PASS, DB_NAME);
    $this->dbcon = $conn;
    $conn -> set_charset("utf8");
    if (mysqli_connect_errno()) {
      echo "Failed to connect to MySQL: " . mysqli_connect_error();
      die();
    }
  }
}
