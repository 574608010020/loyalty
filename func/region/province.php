<?php

require_once __DIR__ . '/../Region.php';

$conn = new Region();

$sql = "SELECT * FROM provinces";
$result = $conn->all($sql);

echo json_encode($result, JSON_UNESCAPED_UNICODE);