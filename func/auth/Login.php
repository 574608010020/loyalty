<?php

include __DIR__ . '/../Query.php';
include __DIR__ . '/../curl.php';
include __DIR__ . '/../Lot.php';


class Login {

  protected $id;
  protected $conn;
  protected $lot;
  protected $curl;
  
  public function __construct(int $id) {
    if (empty($id)) {
      throw new \InvalidArgumentException($id);
    }

    $this->id = $id;
    $this->curl = new Curl();
    $this->conn = new Query();
    $this->lot = new Lot();
  }

  private function checkEmailExistLocal($email) {
    $output = 101;
    $conn = $this->conn;
    $lot = $this->lot;
    $result = $conn->select_assoc("SELECT * FROM origin_lots WHERE email = '$email'");

    if (!$result) {
      $output = 200;
      $data['email'] = $email;
      $data['lots'] = 0.0000;
      $data = json_encode($data, JSON_UNESCAPED_UNICODE);

      $lot->store(json_decode($data));
    }

    return $output;
  }

  private function checkEmailExistTraderRoom() {
    $curl = $this->curl;
    
    return $curl->post('/loyalty/user', json_encode(['user_id' => $this->id]));
  }

  public function load() {
    $user_id = 0;
    $user = $this->checkEmailExistTraderRoom();
    if ($user->user_email) {
      $this->checkEmailExistLocal($user->user_email);
      $user_id = $this->id;
    }

    return $user_id;
  }
  
}