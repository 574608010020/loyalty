<?php

include_once('DotENV.php');

use DevCoder\DotEnv;
(new DotEnv(__DIR__ . '/.env'))->load();

define('DB_SERV', getenv('DB_SERV'));
define('DB_USER', getenv('DB_USER'));
define('DB_PASS', getenv('DB_PASS'));
define('DB_NAME', getenv('DB_NAME_REGION'));


class Region {

  private $dbcon;
  
  public function __construct() {
    $conn = mysqli_connect(DB_SERV, DB_USER, DB_PASS, DB_NAME);
    $this->dbcon = $conn;
    $conn -> set_charset("utf8");
    if (mysqli_connect_errno()) {
      echo "Failed to connect to MySQL: " . mysqli_connect_error();
      die();
    }
  }

  public function query($sql) {
    return mysqli_query($this->dbcon, $sql);
  }

  public function all($sql) {
    return mysqli_fetch_all($this->query($sql), MYSQLI_ASSOC);
  }

  public function assoc($sql) {
    return mysqli_fetch_assoc($this->query($sql));
  }
  
}