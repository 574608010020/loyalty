<?php

ob_start();

require_once 'encrypt.php';

include_once('DotENV.php');
include('function.php');

use DevCoder\DotEnv;
(new DotEnv(__DIR__ . '/.env'))->load();


$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => 'https://servicefx.gofx.com/v1/loyalty/login',
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => '',
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => 'POST',
  CURLOPT_POSTFIELDS =>'{
  "key": "VnV5QU5FVEdPclZhWUlLMzRHaVhvZz09OjrQRhaxqDW2J5kSc6IZtLw4",
  "email": "' . $_POST['email'] . '"
}',
  CURLOPT_HTTPHEADER => array(
    'Content-Type: application/json'
  ),
));

$response = curl_exec($curl);

curl_close($curl);
$json = json_decode($response);

$email = $_POST['email'];
if ($json->code === 200) {
  $CntUser = new CheckUser();
  $objCntUser = $CntUser->fncCntUser($email);
  $cnt = $objCntUser['cnt'];

  if($cnt != 0){
    $obj = new Encrypt();
    $data = $obj->keyData($json->result[0]->user_id);
    header('Location: ' . $_ENV['APP_URL'] . '/curl.php?' . "user=" . $data['user_id'] . '&date=' . $data['date']);
  }else{
    $InsertUser = new CheckUser();
    $objInsertUser = $InsertUser->fncInsertUser($email);
  }
} else {
  echo "Invalid email";
}