<?php $page = 'index'; include('template/header_temp.php'); ?>
<style>
.my-element {
  --animate-repeat: 2;
}
</style>
<div class="container mgt-3" id="index-loyalty">
  <main>
    <div class="row reward-index">
      <div class="col-lg-5 col-md-5 col-12 mb-ver-none mb-lan-none tab-ver-pdl40">
        <aside class="sidebar">
          <div class="component">
            <div class="row mgt-65 banner-box loyalty-text-box loyalty-text-1 text-center">
              <div class="col-lg-4 point-block gofx-point mgtb-1">
                <p class="loyalty-text-2"><?= _REWARD_TEXT_1 ?></p>
                <img src="images/point.gif" class="img-fulid text-center">
              </div>
              <div class="col-lg-8 point-block text-center mgt-4 range-point">
                <h3 id="range" data-animate="fadeInUp"> 0 - 1.8K</h3>
                <?= _REWARD_TEXT_3 ?>
              </div>
            </div>
            <div class="row banner-box mgt-2 loyalty-text-box loyalty-text-1 loyalty-level text-center">
              <div class="col-lg-4 point-block mgtb-1">
                <p class="loyalty-text-2"><?= _REWARD_TEXT_2 ?></p>
                <img src="<?php echo $_ENV['APP_STORAGE']; ?>/images/status/GO-Trader.gif" class="img-fulid" id="loyalty_image">
              </div>
              <div class="col-lg-8 point-block mgt-4">
                <h3 id="loyalty_status"> GO Trader </h3>
                <p id="lot_status"> 1 Lot = 10 Points </p>
              </div>
            </div>
          </div>
        </aside>
        </div>
      <div class="col-lg-7 col-md-7 col-12 index-timeline-pd">
        <article class="article">
          <!-- partial:index.partial.html -->
          <section class="block-content t-block-teal l-block-spacing">
            <div class="l-contained">
          		<ul class="timeline-list">
                <li class="arrow passline firstitem">
                  <div class="content mgb-25">
                    <div class="start_list"></div>
                  </div>
                </li>
                <?php
                  $CntGetLevel = new Index();
                  $objCntGetLevel = $CntGetLevel->fncCntGetLevel($email);
                  $cnt = $objCntGetLevel['cnt'];
 
                  $getlevel = new Index();
                  $objGetLevel = $getlevel->fncGetLevel($email);
                  $i = 1;
                  $numResult = mysqli_num_rows($objGetLevel);
                  while($row = mysqli_fetch_array($objGetLevel)){
                    if($i == $numResult){
                      $margin = 'content';
                    }else{
                      $margin = 'content mgb-30';
                    }

                    switch ($row["reward"]) {
                      case 1:
                        $class = 'pass';
                        break;

                      default:
                        $class = 'lock';
                        break;
                    }

                    switch ($i) {
                      case 1:
                        $id_active = "go";
                        $timeline_tb_mg = "";
                        $animate = '';
                        $title = _REWARDTIMELINE_TEXT_GO;
                        $descript = _REWARDTIMELINE_Go;
                        $descript_mb = _REWARDTIMELINE_MB_Go;
                        $lot_text = _REWARD_LOT_GO;
                        $point_text = _REWARD_POINT_GO;
                        $mb_class = "mb-range-blog mb-range-blog-first";
                        break;

                      case 2:
                        $id_active = "advance";
                        $timeline_tb_mg = "";
                        $animate = 'class="animate animate__slow" data-animate="zoomIn"';
                        $title = _REWARDTIMELINE_TEXT_ADVANCE;
                        $descript = _REWARDTIMELINE_Advance;
                        $descript_mb = _REWARDTIMELINE_MB_Advance;
                        $lot_text = _REWARD_LOT_ADVANCE;
                        $point_text = _REWARD_POINT_ADVANCE;
                        $mb_class = "mb-range-blog";
                        break;

                      case 3:
                        $id_active = "expert";
                        $timeline_tb_mg = "";
                        $animate = 'class="animate animate__slow" data-animate="zoomIn"';
                        $title = _REWARDTIMELINE_TEXT_EXPERT;
                        $descript = _REWARDTIMELINE_Expert;
                        $descript_mb = _REWARDTIMELINE_MB_Expert;
                        $lot_text = _REWARD_LOT_EXPERT;
                        $point_text = _REWARD_POINT_EXPERT;
                        $mb_class = "mb-range-blog";
                        break;

                      default:
                        $id_active = "master";
                        $timeline_tb_mg = "timeline_tb_mg";
                        $animate = 'class="animate animate__slow" data-animate="zoomIn"';
                        $title = _REWARDTIMELINE_TEXT_MASTER;
                        $descript = _REWARDTIMELINE_Master;
                        $descript_mb = _REWARDTIMELINE_MB_Master;
                        $lot_text = _REWARD_LOT_MASTER;
                        $point_text = _REWARD_POINT_MASTER;
                        $mb_class = "mb-range-blog";
                        break;
                    }

                    if($i < $cnt){
                      $class = 'pass passline';
                    }
                ?>
                  <li class="mgt-4 <?= $class,' ',$timeline_tb_mg; ?>">
                    <div <?= $animate; ?>>
                        <div class="<?= $margin ?>">
                          <img src="<?php echo $_ENV['APP_STORAGE']; ?>/<?= $row['level_reward'] ?>" class="img-fulid level_reward_img">
                          <h3 class="level_reward_timeline-text">
                            <span class="reward-timeline-text"><?= $title ?></span><br class="tab-ver tab-lan mb-ver mb-lan">
                          </h3>
                          <p class="mb-ver-none mb-lan-none"><?= $descript ?></p>
                          <div class="<?= $mb_class ?> mgb-1">
                            <div class="row banner-box mgt-1 loyalty-text-box loyalty-text-1 loyalty-level mb-range-blog-pd text-center">
                              <div class="col-4 point-block">
                                <img src="<?php echo $_ENV['APP_STORAGE']; ?>/<?= $row['level_image'];  ?>" class="img-fulid" id="loyalty_image">
                              </div>
                              <div class="col-8 point-block">
                                <p class="loyalty-text-2"><?= _REWARD_TEXT_4 ?></p>
                                <p class="lot_status" id="lot_status"><?= $lot_text; ?></p>
                              </div>
                              <div class="col-4 point-block gofx-point">
                                <img src="images/point.gif" class="img-fulid text-center mb-point-img">
                              </div>
                              <div class="col-8 point-block text-center range-point">
                                <p class="loyalty-text-2"><?= _REWARD_TEXT_1 ?></p>
                                <h3 id="range" data-animate="fadeInUp"> <?= $point_text; ?></h3>
                              </div>
                            </div>
                          </div>
                          <button class="btn btn-danger btn-lg text-center timeline-button" onclick="RedirectToReward('<?= $id_active; ?>')"><?= _REWARD_BUTTON ?></button>
                        </div>
                    </div>
                  </li>
                <?php
                  $i++; }
                ?>
          		</ul>
          	</div>
          </section>
          <!-- partial -->
        </article>
      </div>
    </div>
  </main>
</div>

<div class="container index-content-footer">

  <div class="loyalty-video mgt-10 mgb-3 ">
    <h3><?= _LOYALTY_VIDEO ?></h3>
    <h4><?= _LOYALTY_VIDEO_TEXT ?></h4>
    <div class="videoWrapper">
      <iframe width="560" height="315" src="https://www.youtube.com/embed/IUMVEiMoxZk?autoplay=1&mute=1&loop=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
    </div>
  </div>

  <div class="loyalty-slider mgt-5 display-none">
    <h3><?= _LOYALTY_SLIDER ?></h3>
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
      <ol class="carousel-indicators">
        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner">
        <div class="carousel-item active">
          <img class="d-block w-100" src="<?php echo $_ENV['APP_STORAGE']; ?>/images/img/01.jpg" alt="First slide">
        </div>
        <div class="carousel-item">
          <img class="d-block w-100" src="<?php echo $_ENV['APP_STORAGE']; ?>/images/img/02.jpg" alt="Second slide">
        </div>
        <div class="carousel-item">
          <img class="d-block w-100" src="<?php echo $_ENV['APP_STORAGE']; ?>/images/img/03.jpg" alt="Third slide">
        </div>
      </div>
      <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
  </div>

  <div class="loyalty-about-us mgt-3 mgb-3 display-none">
    <div class="row">
      <div class="col-lg-6 col-md-6 col-12 mb-ver-none">
        <img src="<?php echo $_ENV['APP_STORAGE']; ?>/images/about/office.jpg" class="img-fulid">
      </div>
      <div class="col-lg-6 col-md-6 col-12">
        <h4><?= _LOYALTY_ABOUTUS ?></h4>
        <p><?= _LOYALTY_ABOUTUS_TEXT ?></p>
      </div>
    </div>
  </div>

</div>

<?php include('template/footer_temp.php') ?>
<script>
  $(document).ready(function(){
    localStorage.removeItem('id_active');
  });

  function RedirectToReward(id_active){
    localStorage.setItem('id_active', id_active);
    document.cookie = "id_active="+ id_active;
    window.location = 'rewards';
  }
</script>
