<?php

require_once __DIR__ . '/../func/Query.php';


$conn = new Query();

$sql = "SELECT email, lots, `level`,
        (calPoint(
          IF (isLotDev(email) > 0, isLotDev(email), lots)
        ) - ROUND(calResetPoint(email, lots), 4)) AS `point`,
        timeline
        FROM view_no_trading";
$result = $conn->select_all($sql);
foreach ($result as $row) {
  $fields = implode(",", array_keys($row));
  $values = "'" . implode("','", $row) . "'";

  $sql = "SELECT id, lot as old_lot, `point` as old_point FROM users WHERE email = '$row[email]'";
  $old = $conn->select_assoc($sql);

  $sql = "INSERT INTO `reset` ($fields) VALUES ($values)";
  $conn->query_data($sql);

  $sql = "SELECT id, lot as new_lot, `point` as new_point FROM users WHERE email = '$row[email]'";
  $new = $conn->select_assoc($sql);
  
  unset($row['lots'], $row['point'], $row['timeline']);
  $row['old_lot'] = $old['old_lot'];
  $row['new_lot'] = $new['new_lot'];
  $row['old_point'] = $old['old_point'];
  $row['new_point'] = $new['new_point'];
  $log['user_id'] = $old['id'];
  $log['type'] = "reset";
  $log['value'] = json_encode($row, JSON_UNESCAPED_UNICODE);
  $fields = implode(',', array_keys($log));
  $values = "'" . implode("','", $log) . "'";
  $sql = "INSERT INTO `log` ($fields) VALUES ($values)";
  $conn->query_data($sql);

  unset($log, $fields, $values);
  $log['customer_id'] = $old['id'];
  $log['before_lot'] = $row['old_lot'];
  $log['after_lot'] = $row['new_lot'];
  $fields = implode(',', array_keys($log));
  $values = implode("','", $log);
  $values = "'" . $values . "'";

  $conn->query_data("INSERT INTO log_reset ($fields) VALUES ($values)");
  unset($log, $fields, $values);
}

$sql = "UPDATE dev_lots SET created_at = NOW() WHERE DATE_ADD(DATE(created_at), INTERVAL 6 MONTH) < CURDATE()";
$conn->query_data($sql);

$sql = "UPDATE origin_lots SET created_at = NOW() WHERE DATE_ADD(DATE(created_at), INTERVAL 6 MONTH) < CURDATE()";
$conn->query_data($sql);