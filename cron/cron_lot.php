<?php

require_once '../func/curl.php';
require_once '../func/Query.php';
require_once '../func/Lot.php';
include_once('../func/DotENV.php');

use DevCoder\DotEnv;
(new DotEnv('../func/.env'))->load();

define('KEY', $_ENV['API_KEY']);

$lot = new Lot();
$curl = new Curl();
$query = new Query();


$sql = "SELECT MAX(created_at) as created_at FROM origin_lots";
$ldate = $query->select_assoc($sql)['created_at'];

// Set Param
$param['key'] = getenv('API_KEY');
$param['date'] = getenv('DATE_START');
$param['reset'] = "true";


$lots = $curl->post('/loyalty/lots', json_encode($param, JSON_UNESCAPED_UNICODE));
foreach ($lots as $row) {
  $lot->store($row);
}