<?php $page = 'term-and-conditions'; include('template/header_temp.php'); ?>
<? (isset($_SESSION['lang']) ? $lang = $_SESSION['lang'] : $lang = 'th'); ?>
<div class="container term-content mgtb-3 mgt-3">
  <div class="row mgb-2">
    <div class="col-lg-5 col-md-5 col-12 ">
      <img src="images/term/lyy.png" class="img-fulid">
    </div>
    <div class="col-lg-7 col-md-7 col-12 term-alert">
      <p class="term-heading-1 mgb-08">
        <?= _TERM_AND_CONDITIONS_HEAD_1; ?>
      </p>
      <p class="term-text term-text-indent tab-lan-none tab-var-none mb-lan-none">
        <?= _TERM_AND_CONDITIONS_HEAD_2; ?>
      </p>
      <p class="term-text term-text-indent">
        <?= _TERM_AND_CONDITIONS_HEAD_3; ?>
      </p>
    </div>
  </div>

  <div class="mgb-2">
    <h4 class="term-heading-2 mgb-05">
      <?= _TERM_AND_CONDITIONS_DETAIL_1; ?>
    </h4>
    <div class="term-list-div-1">
      <p class="term-text">
        <?= _TERM_AND_CONDITIONS_DETAIL_2; ?>
      </p>
      <p class="term-text">
        <?= _TERM_AND_CONDITIONS_DETAIL_3; ?>
      </p>
    </div>
  </div>

  <div class="mgb-2">
    <h4 class="term-heading-2 mgb-05">
      <?= _TERM_AND_CONDITIONS_DETAIL_4; ?>
    </h4>
    <div class="term-list-div-3">
      <p class="term-text term-word-text">
        <?= _TERM_AND_CONDITIONS_DETAIL_5; ?>
      </p>
    </div>
  </div>

  <div class="mgb-2">
    <h4 class="term-heading-2 mgb-05">
      <?= _TERM_AND_CONDITIONS_ARTICLE; ?>
    </h4>

    <div class="term-list-div-1 mgb-05">
      <p class="term-text mgb-1">
        <?= _TERM_AND_CONDITIONS_ARTICLE_1; ?>
      </p>

      <p class="term-text mgb-05">
        <?= _TERM_AND_CONDITIONS_ARTICLE_2; ?>
      </p>
    </div>

    <div class="term-list-div-2">
      <div class="mgb-2">
        <p class="term-text mgb-1">
          <?= _TERM_AND_CONDITIONS_ARTICLE_2_1; ?>
        </p>
        <?= ($lang == 'th' ? '<img src="images/term/normal_th.png" class="img-fulid term-cal-point">' : '<img src="images/term/normal_en.png" class="img-fulid term-cal-point">') ?>
      </div>

      <p class="term-text mgb-2">
        <?= _TERM_AND_CONDITIONS_ARTICLE_2_2; ?>
      </p>
      <div class="mgb-2">
        <p class="term-text mgb-1">
          <?= _TERM_AND_CONDITIONS_ARTICLE_2_3; ?>
        </p>
        <?= ($lang == 'th' ? '<img src="images/term/low-spread_th.png" class="img-fulid term-cal-point">' : '<img src="images/term/low-spread_en.png" class="img-fulid term-cal-point">') ?>
        <p class="term-text mgb-1 mgt-1 text-center">
          <?= _TERM_AND_CONDITIONS_ARTICLE_2_3_DETAIL_2; ?>
        </p>
      </div>
      <div class="mgb-2">
        <p class="term-text mgb-1">
          <?= _TERM_AND_CONDITIONS_ARTICLE_2_4; ?>
        </p>
        <?= ($lang == 'th' ? '<img src="images/term/mini_th.png" class="img-fulid term-cal-point">' : '<img src="images/term/mini_en.png" class="img-fulid term-cal-point">') ?>
      </div>
    </div>
  </div>

  <div class="term-list-div-1 mgb-2">
    <p class="term-text  mgb-2">
      <?= _TERM_AND_CONDITIONS_ARTICLE_3; ?>
    </p>

    <p class="term-text  mgb-2">
      <?= _TERM_AND_CONDITIONS_ARTICLE_4; ?>
    </p>

    <p class="term-text  mgb-2">
      <?= _TERM_AND_CONDITIONS_ARTICLE_5; ?>
    </p>

    <p class="term-text  mgb-2">
      <?= _TERM_AND_CONDITIONS_ARTICLE_6; ?>
    </p>
  </div>

  <div class="term-list-div-2 mgb-2">
    <p class="term-text mgb-2">
      <?= _TERM_AND_CONDITIONS_ARTICLE_6_1; ?>
    </p>
  </div>

  <div class="term-list-div-1 mgb-2">
    <p class="term-text mgb-2">
      <?= _TERM_AND_CONDITIONS_ARTICLE_7; ?>
    </p>
  </div>

  <ol class="term-list">
    <li seq="7.1"><?= _TERM_AND_CONDITIONS_ARTICLE_7_1; ?></li>
    <li seq="7.2"><?= _TERM_AND_CONDITIONS_ARTICLE_7_2; ?></li>
    <li seq="7.3"><?= _TERM_AND_CONDITIONS_ARTICLE_7_3; ?></li>
    <li seq="7.4"><?= _TERM_AND_CONDITIONS_ARTICLE_7_4; ?></li>
    <li seq="7.5"><?= _TERM_AND_CONDITIONS_ARTICLE_7_5; ?></li>
    <li seq="7.6"><?= _TERM_AND_CONDITIONS_ARTICLE_7_6; ?></li>
    <li seq="7.7"><?= _TERM_AND_CONDITIONS_ARTICLE_7_7; ?></li>
    <li seq="7.8"><?= _TERM_AND_CONDITIONS_ARTICLE_7_8; ?></li>
    <li seq="7.9"><?= _TERM_AND_CONDITIONS_ARTICLE_7_9; ?></li>
    <li seq="7.10"><?= _TERM_AND_CONDITIONS_ARTICLE_7_10; ?></li>
  </ol>

  <div class="term-list-div-1 mgb-2">
    <p class="term-text mgb-2">
      <?= _TERM_AND_CONDITIONS_ARTICLE_8; ?>
    </p>
  </div>

  <ol class="term-list">
    <li seq="8.1"><?= _TERM_AND_CONDITIONS_ARTICLE_8_1; ?></li>
    <li seq="8.2"><?= _TERM_AND_CONDITIONS_ARTICLE_8_2; ?></li>
    <li seq="8.3"><?= _TERM_AND_CONDITIONS_ARTICLE_8_3; ?></li>
    <li seq="8.4"><?= _TERM_AND_CONDITIONS_ARTICLE_8_4; ?></li>
  </ol>

</div>
<?php include('template/footer_temp.php') ?>
<script>
  $(document).ready(function(){
    localStorage.removeItem('id_active');
  });
</script>
